import 'package:flutter/material.dart';

import '../constant.dart';


class BottomButton extends StatelessWidget {
  // ignore: prefer_const_constructors_in_immutables
  BottomButton({Key? key, required this.onTap, required this.buttonTitle}) : super(key: key);

  final Function() onTap;
  final String buttonTitle;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        color: kBottomContainerColour,
        margin: const EdgeInsets.only(top: 10.0),
        padding: const EdgeInsets.only(bottom: 13.0),
        width: double.infinity,
        height: kBottomContainerHeight,
        child: Center(
          child: Column(
            children: [
              const SizedBox(height: 17,),
              Text(
                buttonTitle,
                style: kLargeButtonTextStyle,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
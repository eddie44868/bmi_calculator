import 'package:flutter/material.dart';

class RoundIconButton extends StatelessWidget {
  // ignore: prefer_const_constructors_in_immutables
  RoundIconButton({Key? key, required this.icon, required this.onPress}) : super(key: key);

  final IconData icon;
  final Function() onPress;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      elevation: 0.0,
      onPressed: onPress,
      constraints: const BoxConstraints.tightFor(
        width: 56,
        height: 56,
      ),
      shape: const CircleBorder(),
      fillColor: const Color(0xff4C4F5E),
      child: Icon(icon),
    );
  }
}